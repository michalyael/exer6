<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;


/**
 * DemoController implements the CRUD actions for Demo model.
 */
class UserController extends Controller
{
    public function actionIndex()
    {
        return $this->goHome();	
    }

    public function actionAdd()
    {
		$user = new User();
		$user->username = "rotemze";
		$user->password = "76153";
		$user->save();
		
		$user = new User();
		$user->username = "davidan";
		$user->password = "57301";
		$user->save();
		
		$user = new User();		
		$user->username = "ron";
		$user->password = "54321";
		$user->save();
		
		$user = new User();		
		$user->username = "arya";
		$user->password = "12345";
		$user->save();
		
		$user = new User();		
		$user->username = "liora";
		$user->password = "98765";
		$user->save();
				
		return $this->goHome();			
    }
}